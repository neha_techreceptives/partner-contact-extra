# -*- coding: utf-8 -*-
# © 2019 Flectra Community (https://www.flectra-community.org)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).
{
    'name': 'Partner Copy Address',
    'summary': 'Partner Copy Address',
    'version': '1.0',
    'author': 'Flectra Community',
    'license': "AGPL-3",
    'maintainer': 'Flectra Community',
    'category': 'Extra Tools',
    'website': 'https://www.flectra-community.org',
    'depends': ['base'],
    'data': ['views/res_partner_view.xml',
             'views/asset.xml'
    ],
    'installable': True,
    'auto_install': False,
}