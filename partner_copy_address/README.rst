===========
Partner Copy Address
===========

This module adds the featrue to copy partners address to clipboard.

**Table of contents**

.. contents::
   :local:

Usage
=====

To use this module, you need to:

#. Go to the partner form
#. Click the Copy Adress button
#. Paste copied data anywhere you need

Credits
=======

Authors
~~~~~~~

* Flectra Community

Contributors
------------

* Flectra Community <info@flectra-community.org>
